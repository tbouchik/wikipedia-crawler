from urllib.request import urlopen
from lxml import etree, html
from queue import Queue
from threading import Thread
import sys, traceback, re, os, time



base_url = "https://en.wikipedia.org"
number_of_links = 10
output_file = 'output'
number_of_threads = 8
file_path = 'keywords.csv'

class AttributeFilter(etree.TreeBuilder):
    def start(self, tag, attrib):
        attrib = dict(attrib)
        return super(AttributeFilter, self).start(tag, attrib)


class WikiCrawler(object):
    
    def __init__(self, keywords):
        super().__init__()
        self.keywords = keywords

    
    @staticmethod
    def keywords_to_search(filePath):
        try:
            with open(filePath, 'r') as rf:
                return [elt.replace('\n', '') for elt in rf.readlines()]
        except: 
            print('An error occured when reading the file:')
            #traceback.print_exc(file=sys.stdout)
    
    @staticmethod
    def write_file(path, data):
        with open(path, 'a+') as f:
            f.write('\n'+ data +'\n')

    @staticmethod
    def create_dir(name):
        if not os.path.isdir(name):
            os.mkdir(name)


    def link_fetcher(self, keywordsList):
        for elt in keywordsList:
            url1 = 'https://en.wikipedia.org/w/index.php?search={}&title=Special%3ASearch&profile=default&fulltext=1'.format(elt)
            try:
                with urlopen(url1) as url_file:
                    s = url_file.read()
                    parser = etree.XMLPullParser(target=AttributeFilter())
                    parser.feed(s)
                    numberOfLinks = number_of_links
                    for _, element in parser.read_events():
                        if 'data-serp-pos' in element.attrib.keys() and numberOfLinks > 0:
                            numberOfLinks -= 1
                            yield(elt, element.attrib["href"])
            except:
                #traceback.print_exc(file=sys.stdout)
                continue

    def data_fetcher(self, keyword, url):
        pattern = re.compile(r'.*[^[][0-9][^]][^\.]*.') #TODO: change nothing else than [ and number
        completeUrl = base_url + url
        with urlopen(completeUrl) as url_file:
            s = url_file.read()
            html_content = html.fromstring(s)
            tag1 = html_content.xpath('//p')
            for elt in tag1:
                try:
                    content= elt.text_content()
                    if content is not None: 
                        sentencesWithNumber = pattern.findall(content)
                        for match in sentencesWithNumber:
                            WikiCrawler.write_file(output_file+'/'+keyword+'.txt', match)
                        
                except:
                    #traceback.print_exc(file=sys.stdout) #TODO perf
                    continue

    def runSearch(self):
        for elt in self.link_fetcher(self.keywords):
            self.data_fetcher(elt[0], elt[1])

################## Multi-threading ####################
def launch_crawler(keywords):
    crawler = WikiCrawler(keywords)
    crawler.runSearch()

def divide_into_chunks(l, n):
    """Yield successive n-sized chunks from l."""
    for i in range(0, len(l), n):
        yield l[i:i + n]

def main():
    threadlist = []
    WikiCrawler.create_dir(output_file)
    keywordsList = WikiCrawler.keywords_to_search(file_path)
    length = len(keywordsList)
    chunks = [keywordsList[x:x+number_of_threads] for x in range(0, len(keywordsList), number_of_threads)]
    for thread_k in range(len(chunks)):
        t = Thread(target=launch_crawler, args=(chunks[thread_k], ))
        t.daemon =True
        t.start()
        threadlist.append(t)
    for th in threadlist:
        th.join()


if __name__ == "__main__":
    # I/O start
    print("========== Very simple crawler for Wikipedia Search ===========\n")
    print("================== Taoufik Bouchikhi - 2019 ==================\n")
    name = "None"
    while name not in 'yYnN':
        name = input("Is the name of keywords file 'keywords.csv'? [y/n]: ")     
    if name in "nN":
        file_path = input("Please input the path to the file containing the keywords:  ")
    number_of_threads = int(input("How many threads do you want to use?:  "))
    number_of_links = int(input("How many links for each keyword do you want to crawl?:  "))
    print('the output is in preparation in the "output" directory ...')
    start_time = time.time() 
    main()
    print("--- %s seconds ---" % (time.time() - start_time))

